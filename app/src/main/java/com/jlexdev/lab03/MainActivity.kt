package com.jlexdev.lab03

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Setup Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayShowHomeEnabled(true)
        }

        toolbarLayout.apply {
            title = "Desarrollo Móvil"
            setCollapsedTitleTextColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
            setExpandedTitleColor(ContextCompat.getColor(applicationContext, R.color.colorWhite))
        }

    }
}